import React, { Component } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';



 class App extends Component {

    constructor(props) {
        super(props);

        this.state = {
            products: [{
              code:'XLSX',
            
              bid:'321.02',
              totalbid:'230.0'
            }]
        };

       
    }
   randomNumberInRange(min, max) {
      // 👇️ get number between min (inclusive) and max (inclusive)
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    componentDidMount() {
      this.interval = setInterval(() => this.genRandom(), 5000);
    }
    genRandom(){
var bid=this.randomNumberInRange(10,100);
var totalbid=this.randomNumberInRange(100,200);
var state=this.state;
state.products.push({code:'YEY',bid:bid,totalbid:totalbid});
this.setState({state});
    }

  

    render() {
        return (
            <div>
                <div className="card">
                    <DataTable value={this.state.products} header="SMC ASSESSMENT" responsiveLayout="scroll">
                        <Column field="code" header="Code" />
                       
                        <Column field="bid" header="Bid" />
                        <Column field="totalbid" header="Total Bid" />
  
                    </DataTable>
                </div>

              
            </div>
        );
    }
}
export default App;